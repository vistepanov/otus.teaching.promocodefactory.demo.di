﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.ConsoleHost.Dto;
using Otus.Teaching.PromoCodeFactory.ConsoleHost.Views;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.ConsoleHost.Controllers
{
    public class EmployeeConsoleController
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeeConsoleController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        public async Task GetEmployeesListAsync()
        {
            Console.WriteLine($"Controller with code: {GetHashCode()}");
            
            var employees = await _employeeRepository.GetAllAsync();

            var employeesList = employees.Select(x =>
                new EmployeeShortInfoDto()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            var view = new EmployeeListView(employeesList);
            view.Show();
        }
    }
}